//
//  RepoDetailsPresenter.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation

protocol RepoDetailsPresentation: class {
    func configureView(with item: RepoItem)
}

protocol RepoDetailsPresenterProtocol {
    func viewDidload()
}

final class RepoDetailsPresenter: RepoDetailsPresenterProtocol {
    
    private weak var view: RepoDetailsPresentation!
    private var item : RepoItem!
    
    init(
        view: RepoDetailsPresentation,
        item: RepoItem
    ) {
        self.view = view
        self.item = item
    }
    
    func viewDidload() {
        view.configureView(with: item)
    }
}
