//
//  RepoListInteractor.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

typealias RepoListResult = (Result<[RepoModel],Error>)

protocol RepoListInteractorProtocol {
    func getRepoList(handler :@escaping (RepoListResult) -> Void )
}

final class RepoListInteractor: RepoListInteractorProtocol {
    
    func getRepoList(handler: @escaping (RepoListResult) -> Void) {
        PerformNetwork.sendRequest(model: [RepoModel].self, url: .repositories, completion: handler)
    }
}
