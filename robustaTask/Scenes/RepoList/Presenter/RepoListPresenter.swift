//
//  RepoListPresenter.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation

protocol RepoListPresentation: class {
    func showLoader()
    func dismissLoader()
    func showError(error : String)
    func reloadData()
}

protocol RepoListPresenterProtocol {
    var numberOfRows: Int { get }
    
    func fetchData()
    func search(query: String)
    func resetSearchResult()
    func configure(cell: RepoListCellPresentable, for indexPath: IndexPath )
    func didSelectRowAt(indexPath: IndexPath)
}

final class RepoListPresenter{
    
    private weak var view: RepoListPresentation!
    private var interactor: RepoListInteractorProtocol!
    private var router: RepoListRouterProtocol!
    private var originalItems = [RepoItem]() { didSet { items = originalItems } }
    private var items = [RepoItem]() { didSet { view.reloadData() } }
    
    init(
        view: RepoListPresentation,
        interactor : RepoListInteractorProtocol,
        router: RepoListRouterProtocol
    ) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}
// MARK:- RepoListPresenterProtocol
extension RepoListPresenter: RepoListPresenterProtocol {
    
    var numberOfRows: Int { items.count }
    
    func configure(cell: RepoListCellPresentable, for indexPath: IndexPath ) {
        cell.configure(item: items[indexPath.row])
    }
    
    func didSelectRowAt(indexPath: IndexPath) {
        router.showRepoDetails(item: items[indexPath.row])
    }
    
    func search(query: String) {
        items =  originalItems.filter { $0.name == query }
        // if you want get with contains those character you can use
        //items =  originalItems.filter { $0.name.contains(query) }
        
    }
    
    func resetSearchResult() {
        items = originalItems
    }
    
    func fetchData() {
        view.showLoader()
        interactor.getRepoList { [weak self] result in
            guard let self = self else { return }
            self.view.dismissLoader()
            switch result {
            case .success(let model):
                self.originalItems = model.compactMap { RepoItem(model: $0)}
            case .failure(let error):
                self.view.showError(error: error.localizedDescription)
            }
        }
    }
}
