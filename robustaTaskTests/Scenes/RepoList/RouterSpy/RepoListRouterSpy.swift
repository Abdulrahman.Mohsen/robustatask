//
//  RepoListRouterSpy.swift
//  robustaTaskTests
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation
@testable import robustaTask

final class RepoListRouterSpy: RepoListRouterProtocol {
    
    var showRepoDetailsCallCount = 0
    var passedItem: RepoItem?
    
    func showRepoDetails(item: RepoItem) {
        showRepoDetailsCallCount += 1
        passedItem = item
    }
}
