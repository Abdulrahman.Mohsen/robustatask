//
//  RepoListPresenterTests.swift
//  robustaTaskTests
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import XCTest
@testable import robustaTask

class RepoListPresenterTests: XCTestCase {
    
    var viewSpy: RepoListViewSpy!
    var stub: RepoListInteractorStub!
    var routerSpy: RepoListRouterSpy!
    var presenter: RepoListPresenter!
    var cellSpy: RepoListCellSpy!

    override func setUp() {
        super.setUp()
        viewSpy = RepoListViewSpy()
        routerSpy = RepoListRouterSpy()
        stub = RepoListInteractorStub()
        cellSpy = RepoListCellSpy()
        presenter = RepoListPresenter(view: viewSpy, interactor: stub, router: routerSpy)
    }

    override func tearDown() {
        viewSpy = nil
        stub = nil
        cellSpy = nil
        presenter = nil
        super.tearDown()
    }
    
    func testFetchDataSuccess() {
        //Given
        let items = [RepoModel(id: 0, name: "name", githubPrivate: false, owner: nil, githubDescription: "githubDescription", fork: false)]
        let indexPath = IndexPath(row: 0, section: 0)
        let expectedItem = RepoItem(model: items[indexPath.row])
        stub.repoListResultToBeReturn = .success(items)
        //When
        presenter.fetchData()
        presenter.configure(cell: cellSpy, for: indexPath)
        
        //Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.reloadDataCallCount, 1)
        XCTAssertEqual(presenter.numberOfRows, items.count)
        XCTAssertEqual(cellSpy.configureCallCount, 1)
        XCTAssertEqual(cellSpy.passedItem, expectedItem)
    }
    
    func testFetchDataFail() {
        //Given
        let error = ApiError.missingData
        stub.repoListResultToBeReturn = .failure(error)
        //When
        presenter.fetchData()
        
        //Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.showErrorCallCount, 1)
        XCTAssertEqual(viewSpy.passedError, error.localizedDescription)
    }
    
    func testDidSelectRow() {
        //Given
        let items = [RepoModel(id: 0, name: "name", githubPrivate: false, owner: nil, githubDescription: "githubDescription", fork: false)]
        let indexPath = IndexPath(row: 0, section: 0)
        let expectedItem = RepoItem(model: items[indexPath.row])
        stub.repoListResultToBeReturn = .success(items)
        //When
        presenter.fetchData()
        presenter.didSelectRowAt(indexPath: indexPath)
        
        //Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.reloadDataCallCount, 1)
        XCTAssertEqual(presenter.numberOfRows, items.count)
        XCTAssertEqual(routerSpy.showRepoDetailsCallCount, 1)
        XCTAssertEqual(routerSpy.passedItem, expectedItem)
    }
    
    func testSearch() {
        //Given
        let items = [RepoModel(id: 0, name: "name1", githubPrivate: false, owner: nil, githubDescription: "githubDescription", fork: false),
                     RepoModel(id: 0, name: "name2", githubPrivate: false, owner: nil, githubDescription: "githubDescription", fork: false)]
        stub.repoListResultToBeReturn = .success(items)
        //When
        presenter.fetchData()
        presenter.search(query: "name1")
        //Then
        XCTAssertEqual(viewSpy.showLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.dismissLoaderCallCount, 1)
        XCTAssertEqual(viewSpy.reloadDataCallCount, 2)
        XCTAssertEqual(presenter.numberOfRows, 1)
        
        //When
        presenter.search(query: "name")
        //Then
        XCTAssertEqual(presenter.numberOfRows, 0)
        XCTAssertEqual(viewSpy.reloadDataCallCount, 3)
        
        //When
        presenter.search(query: "0")
        //Then
        XCTAssertEqual(presenter.numberOfRows, 0)
    }
}
