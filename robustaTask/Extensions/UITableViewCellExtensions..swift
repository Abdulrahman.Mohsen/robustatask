//
//  UIViewControllerExtension.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

extension UITableViewCell {
    
    class var cellId: String {
        return "\(self)"
    }
}
