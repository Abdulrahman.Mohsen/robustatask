//
//  UIViewExtension.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

extension UIView {
    
    static var nib: UINib {
        return UINib(nibName: self.className, bundle: nil)
    }
}
