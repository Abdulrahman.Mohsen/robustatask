//
//  RepoModel.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation

struct RepoItem: Equatable {
    
    let id: Int
    let name: String
    let githubPrivate: Bool
    let owner: OwnerItem
    let githubDescription: String
    let fork: Bool
    
    init(model: RepoModel) {
        id = model.id ?? 0
        name = model.name ?? ""
        githubPrivate = model.githubPrivate ?? false
        owner = OwnerItem(model: model.owner)
        githubDescription = model.githubDescription ?? ""
        fork = model.fork ?? false
    }
    
    static func == (lhs: RepoItem, rhs: RepoItem) -> Bool {
        return  lhs.id == rhs.id
    }
}

struct OwnerItem {
    
    let login: String
    let avatarURL: String
    let type: TypeEnum
    
    init(model: Owner?) {
        login = model?.login ?? ""
        avatarURL = model?.avatarURL ?? ""
        type = model?.type ?? .user
    }
}

struct RepoModel: Codable {
    
    let id: Int?
    let name: String?
    let githubPrivate: Bool?
    let owner: Owner?
    let githubDescription: String?
    let fork: Bool?
    
    init(
        id: Int?,
        name: String?,
        githubPrivate: Bool?,
        owner: Owner?,
        githubDescription: String?,
        fork: Bool?
    ) {
        self.id = id
        self.name = name
        self.githubPrivate = githubPrivate
        self.owner = owner
        self.githubDescription = githubDescription
        self.fork = fork
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case githubPrivate = "private"
        case owner
        case githubDescription = "description"
        case fork
    }
}

// MARK: - Owner
struct Owner: Codable {
    let login: String?
    let avatarURL: String?
    let type: TypeEnum?
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarURL = "avatar_url"
        case type
    }
}

enum TypeEnum: String, Codable {
    case organization = "Organization"
    case user = "User"
}
