//
//  RepoListInteractorStub.swift
//  robustaTaskTests
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation
@testable import robustaTask

final class RepoListInteractorStub: RepoListInteractorProtocol {
    
    var repoListResultToBeReturn: RepoListResult!

    func getRepoList(handler: @escaping (RepoListResult) -> Void) {
        handler(repoListResultToBeReturn)
    }
}

