//
//  RepoRouter.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

final class RepoDetailsRouter {
    
    static func creatRepoDetails(item: RepoItem) -> UIViewController {
        let controller = RepoDetailsViewController()
        let presenter = RepoDetailsPresenter(view: controller, item: item)
        controller.presenter = presenter
        return controller
    }
}
