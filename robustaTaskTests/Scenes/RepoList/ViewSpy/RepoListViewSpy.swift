//
//  RepoListViewSpy.swift
//  robustaTaskTests
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation
@testable import robustaTask

final class RepoListViewSpy: RepoListPresentation {
    
    var showLoaderCallCount = 0
    var dismissLoaderCallCount = 0
    var showErrorCallCount = 0
    var reloadDataCallCount = 0
    
    var passedError: String?
    
    func showLoader() {
        showLoaderCallCount += 1
    }
    
    func dismissLoader() {
        dismissLoaderCallCount += 1
    }
    
    func showError(error: String) {
        showErrorCallCount += 1
        passedError = error
    }
    
    func reloadData() {
        reloadDataCallCount += 1
    }
}
