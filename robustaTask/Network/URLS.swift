//
//  URLS.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation

let baseURL = "https://api.github.com/"

public enum APIURLs : String {
    
    case repositories = "repositories"
    
    var value : String {
        return baseURL + self.rawValue
    }
    
}
