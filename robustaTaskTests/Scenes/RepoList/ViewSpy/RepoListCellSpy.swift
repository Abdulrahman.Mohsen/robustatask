//
//  RepoListCellSpy.swift
//  robustaTaskTests
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation
@testable import robustaTask

final class RepoListCellSpy: RepoListCellPresentable {
    var configureCallCount = 0
    var passedItem: RepoItem?
    
    func configure(item: RepoItem) {
        configureCallCount += 1
        passedItem = item
    }
}
