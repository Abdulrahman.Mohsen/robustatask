//
//  RepoDetailsViewController.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

class RepoDetailsViewController: UIViewController {
    
    // MARK: - outlets
    @IBOutlet private weak var txtDescription: UITextView!
    @IBOutlet private weak var lblRepoIsFork: UILabel!
    @IBOutlet private weak var lblRepoIsPrivate: UILabel!
    @IBOutlet private weak var lblRepoName: UILabel!
    @IBOutlet private weak var lblType: UILabel!
    @IBOutlet private weak var lblOwnerName: UILabel!
    @IBOutlet private weak var imgOwner: CustomImageView!
    // MARK: - variables
	var presenter: RepoDetailsPresenterProtocol!
    
	override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidload()
        title = "Details"
    }
    
}

// MARK: - RepoDetailsPresentation
extension RepoDetailsViewController: RepoDetailsPresentation {
    
    func configureView(with item: RepoItem) {
        imgOwner.loadImage(urlString: item.owner.avatarURL)
        lblOwnerName.text = item.owner.login
        lblType.text = item.owner.type.rawValue
        lblRepoName.text = item.name
        lblRepoIsPrivate.text = item.githubPrivate.description
        lblRepoIsFork.text = item.fork.description
        txtDescription.text = item.githubDescription
    }
}
