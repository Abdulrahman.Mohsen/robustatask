//
//  RepoListCell.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

protocol RepoListCellPresentable {
    func configure(item: RepoItem)
}

final class RepoListCell: UITableViewCell {

    // MARK: - outlets
    @IBOutlet private weak var lblDate: UILabel!
    @IBOutlet private weak var lblOwnerName: UILabel!
    @IBOutlet private weak var lblRepoName: UILabel!
    @IBOutlet private weak var imgRepoOwner: CustomImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgRepoOwner.image = nil
    }
}

extension RepoListCell: RepoListCellPresentable {
    func configure(item: RepoItem) {
        lblOwnerName.text = item.owner.login
        lblRepoName.text = item.name
        imgRepoOwner.loadImage(urlString: item.owner.avatarURL)
        //Didn't found date To display it
    }
}
