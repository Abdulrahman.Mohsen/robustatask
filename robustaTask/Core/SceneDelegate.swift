//
//  SceneDelegate.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        initalizeWindow()
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    private func initalizeWindow() {
        let repoListViewController = RepoListRouter.createRepoList()
        let navigationController = UINavigationController(rootViewController: repoListViewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

