//
//  RepoListRouter.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

protocol RepoListRouterProtocol: AnyObject {
    func showRepoDetails(item: RepoItem)
}

final class RepoListRouter {
    
    private weak var viewController: RepoListViewController?
    
    init(viewController: RepoListViewController) {
        self.viewController = viewController
    }
    
    static func createRepoList() -> UIViewController {
        let controller = RepoListViewController()
        let interactor = RepoListInteractor()
        let router = RepoListRouter(viewController: controller)
        let presenter = RepoListPresenter(view: controller, interactor: interactor, router: router)
        controller.presenter = presenter
        return controller
    }

}

extension RepoListRouter: RepoListRouterProtocol {
    
    func showRepoDetails(item: RepoItem) {
        let repoDetailsViewController = RepoDetailsRouter.creatRepoDetails(item: item)
        viewController?.show(repoDetailsViewController, sender: nil)
    }
}
