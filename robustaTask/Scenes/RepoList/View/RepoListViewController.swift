//
//  RepoListViewController.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import UIKit

class RepoListViewController: UIViewController {

    // MARK: - outlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    // MARK: - variables
	var presenter: RepoListPresenterProtocol!
    var refreshControl = UIRefreshControl()

	override func viewDidLoad() {
        super.viewDidLoad()
        title = "repositories"
        setupTableView()
        searchBar.delegate = self
        setupRefreshControll()
        presenter.fetchData()
    }
    
    private func setupTableView(){
        tableView.register(cell: RepoListCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupRefreshControll(){
        refreshControl.attributedTitle = NSAttributedString(string: "restore data")
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshAction), for: UIControl.Event.valueChanged)
        refreshControl.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        tableView.refreshControl = refreshControl
    }
    
    @objc private func refreshAction() {
        searchBar.text?.removeAll()
        presenter.resetSearchResult()
        refreshControl.endRefreshing()
    }
    
}

// MARK:- UISearchBarDelegate
extension RepoListViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let search = searchBar.text, search.count > 1 else { return }
        presenter.search(query: search)
    }
}

// MARK:- RepoListPresentation
extension RepoListViewController: RepoListPresentation {
    
    func showLoader(){
        // Display UI Loader To Enhance UX
    }
    
    func dismissLoader(){
        // Display UI Loader To Enhance UX
    }
    
    func showError(error : String){
        print(error)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
}

// MARK:- UITableViewDelegate, UITableViewDataSource
extension RepoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RepoListCell = tableView.dequeueCell(for: indexPath)
        presenter.configure(cell: cell, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath: indexPath)
    }
}

