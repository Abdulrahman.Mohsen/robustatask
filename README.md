# Robusta Task

This project is a coding task by **Abdelrahman mohsen**.   
Technical Task for **Robusta Studio**.

# Brief For Task

- Retrieve repositories from Github endpoint.
- Repo list include owner name, owner Image, repo name, and created date.
- Repo details include some data from the owner and repo.
- Try to avoid reload images over and over by cash images.

# Architecture

 Task flow with viper and this contain.
- View: displays what it is told to by the Presenter and relays user input back to the Presenter. 
- Interactor: contains the business logic as specified by a use case. e.g if business logic depend on making network calls then it is Interactor’s responsibility to do so
- Presenter: contains view logic for preparing content for display (as received from the Interactor) and for reacting to user inputs (by requesting new data from the Interactor).
- Entity: contains basic model objects used by the Interactor.
- Routing: contains navigation logic for describing which screens are shown in which order.







