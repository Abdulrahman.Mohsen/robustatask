//
//  NSObjectExtension.swift
//  robustaTask
//
//  Created by Abulrahman mohsen on 4/10/21.
//

import Foundation

extension NSObject {
    
    class var className: String {
        return "\(self)"
    }
}
